import ChatInputNext from "./components/chat-input-next.vue";

export { ChatInputNext };

const components = [ChatInputNext];

const install = (app: any) => {
  components.forEach((component) => {
    app.component(component.name, component);
  });
};

export default { install };