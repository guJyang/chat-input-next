// 允许接受的类型
export const acceptList:string[]=[
    ".jpg,.jpeg,.png,.gif,.bmp,.JPG,.JPEG,.PBG,.GIF",
    ".doc,.docx,.zip,.txt,.xls,.xlsx,.csv,.md,.rar,.7z,.pdf",
    ".mp4, .flv, .avi, .mov, .wmv"
]

// 需要展示的图片
export const showFileImageList:string[] = [
    new URL("@/assets/word.jpg", import.meta.url).href,
    new URL("@/assets/excel.jpg", import.meta.url).href,
    new URL("@/assets/zip.jpg", import.meta.url).href,
    new URL("@/assets/txt.jpg", import.meta.url).href,
    new URL("@/assets/pdf.jpg", import.meta.url).href,
    new URL("@/assets/video.png", import.meta.url).href,
];