import { CSSProperties } from 'vue';
export type InlineStyles = Partial<CSSProperties>;
export type AtEachMember ={
  name: string;
  id?: string;
  avatar?: string;
};

export type ExportMsg=string|{id?:string,type?:'aov',url?:string}
