# chat-input-next

基于vue3+vite以及div的contenteditable属性实现的聊天输入框组件

## 支持
暂时支持图片，视频，文件等插入，支持图片，视频，文件的复制，@逻辑

## 其他版本
我还用react写了一个版本的这个组件，react版本实现逻辑和vue的版本类似，只是我换了个代码写法，仓库为https://gitee.com/guJyang/chat-input-react

## 使用方式
### 安装
```
npm i chat-input-next
```
### 在组件中使用
```
import {ChatInputNext} from 'chat-input-next'
import "chat-input-next/dist/style.css"
```

### 支持属性
|  属性名   | 用处  |例子|默认值|
|  ----  | ----  |  ----  | ----  |
| cusStyle  | 用来自定义样式 |{width:'80vw',height:'50vh',border:'1px solid #d3d3d3',maxHeight:'50vh'}|-|
| atUserList  | 定义@人员的列表 |[{name: 'codesigner',avatar:'xxxx'},{name: 'react',avatar:'xxxx'}]|-|


### 支持事件
|  方法名   | 用处  |备注|
|  ----  | ----  |  ----  |
| error  | 接收组件报错 |-|
|  insertFile | 允许插入文件  |  需结合ref使用，传值为File类型 |
|  exportData | 导出内容  |  需结合ref使用|

## 实现逻辑
https://gujyang.gitee.io/myblog/2023/09/19/chat-input-next-package/
https://gujyang.gitee.io/myblog/2023/09/21/chat-input-next-package-2/

## 使用案例代码
```
<script setup lang="ts">
import {ChatInputNext} from 'chat-input-next.vue'
import "chat-input-next/dist/style.css"
import {ref} from "vue"

const chatInputNext=ref()
// 上传文件
const handleFileChange = (e:Event)=>{
  const target=e.target as HTMLInputElement
  if(target.files&&target.files.length>0){
    const file=target.files[0]
    chatInputNext.value&&chatInputNext.value.insertFile(file)
  }
}
// 处理报错信息
const handleError=(msg:string)=>{
  console.log(msg)
}
// 得到导出的数据
const handleDataExport=()=>{
  const data=chatInputNext.value&&chatInputNext.value.exportData()
  console.log(data)
}

// @列表
const atPopoverList = [{
    name: 'codesigner',
    avatar:new URL("@/assets/word.jpg", import.meta.url).href,
},
{
    name: 'react',
    avatar: new URL("@/assets/excel.jpg", import.meta.url).href,
},
{
    name: 'vue',
    avatar: new URL("@/assets/video.png", import.meta.url).href,
},
{
    name: 'webgl',
    avatar: new URL("@/assets/pdf.jpg", import.meta.url).href,
},
{
    name: 'gis',
    avatar: new URL("@/assets/txt.jpg", import.meta.url).href,
},{
    name:'cesium',
    avatar: new URL("@/assets/zip.jpg", import.meta.url).href,
},{
    name:'three',
    avatar:new URL("@/assets/word.jpg", import.meta.url).href,
},{
    name:'cannon-es',
    avatar: new URL("@/assets/excel.jpg", import.meta.url).href,
}]
</script>

<template>
  <div class="chat-input-next-container">
    输入框组件:
    <div class="op-line">
      <input type="file"  @change="handleFileChange">
      <button @click="handleDataExport">导出数据</button>
    </div>
    <ChatInputNext :cusStyle="{width:'80vw',height:'50vh',border:'1px solid #d3d3d3',maxHeight:'50vh'}" ref="chatInputNext" @error="handleError" :atUserList="atPopoverList"></ChatInputNext>
  </div>
</template>
<style scoped>
.chat-input-next-container{
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap:10px;
  width: 100vw;
  height: 100vh;
  background-color: #f5f5f5;
}
.op-line{
  display: flex;
  flex-direction: row;
  align-items: center;
  gap:10px;
  width:80vw
}
</style>

```




